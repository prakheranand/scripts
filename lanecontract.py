import pymysql
from decimal import Decimal
import sys, traceback
import csv

#database connection
connection = pymysql.connect(host="finance-prod-replica.c90oafloutlv.ap-southeast-1.rds.amazonaws.com",
                             user="root",
                             passwd="f1n4nc3#r1v1g0",
                             database="contract_management_system" )

# col_of_interest = "select distinct(name) from client_contract_attribute"
#
# cursor = connection.cursor()
# nameSize = cursor.execute(col_of_interest);
# name_of_interest = [];
# valueCursor = connection.cursor();
# z = 0
#
# for row in cursor.fetchall():
#     valueQuery = "select value from client_contract_attribute where name = '" + row[0] + "' limit 1";
#     try :
#         valueCursor.execute(valueQuery);
#         for singlerow in valueCursor.fetchall() :
#             try :
#                 float(singlerow[0]);
#                 if(row[0] == 'BANK_ACCOUNT_NUMBER'):
#                     continue;
#                 name_of_interest.append(row[0]);
#                 z = 1;
#             except Exception as e1 :
#                 z = 0;
#     except Exception as e :
#         print (e);

new_cursor = connection.cursor();

final_list = [];
fp = open('/tmp/prime_contract_route_2.csv', 'w')
fp.write("client_contract_id,name,value");
fp.write("\n");


std_dev_query = "select client_contract.client_code,prime_contract_route.code, charge_basis,contract_route_hash, trip_type , freight_charges,mean, dev,client_contract.service_type from client_contract, prime_contract_route , (select  stddev(freight_charges) as dev, avg(freight_charges) as mean from prime_contract_route where status in('ACTIVE','APPROVED', 'EXPIRED') and charge_basis = 'Higher of Weight or Vehicle Capacity' )as devtable where charge_basis = 'Higher of Weight or Vehicle Capacity' and  ((freight_charges - devtable.mean) > (4 *devtable.dev)  or freight_charges <(devtable.mean * .05)  )and prime_contract_route.status in('ACTIVE','APPROVED', 'EXPIRED') and prime_contract_route.contract_id=client_contract.id";

#std_dev_query = "select client_contract.code, client_contract.service_type, client_contract.client_code,client_contract_id,client_contract_attribute.name,client_contract_attribute.value from client_contract, client, client_contract_attribute , (select  stddev(value) as dev, avg(value) as mean from  client_contract_attribute where name = '"+column+"') as devtable where client_contract_attribute.name = '"+column+"' and  abs((client_contract_attribute.value - devtable.mean)) > (4 *devtable.dev) and client_contract.id = client_contract_attribute.client_contract_id and client_contract.is_active = true and client.code=client_contract.client_code";
print std_dev_query;
new_cursor.execute(std_dev_query);
for newlist in new_cursor.fetchall():
    for entry in newlist :
        fp.write(str(entry))
        fp.write(",")
    fp.write("\n");



std_dev_query = "select client_contract.client_code,prime_contract_route.code, charge_basis,contract_route_hash, trip_type , freight_charges,mean, dev, client_contract.service_type from client_contract, prime_contract_route , (select  stddev(freight_charges) as dev, avg(freight_charges) as mean from prime_contract_route where status in('ACTIVE','APPROVED', 'EXPIRED') and charge_basis = 'Per Trip' )as devtable where charge_basis = 'Per Trip' and  ((freight_charges - devtable.mean) > (6 *devtable.dev)  or (devtable.mean - freight_charges) > (1.5 * devtable.dev) )and prime_contract_route.status in('ACTIVE','APPROVED', 'EXPIRED') and prime_contract_route.contract_id=client_contract.id";

    #std_dev_query = "select client_contract.code, client_contract.service_type, client_contract.client_code,client_contract_id,client_contract_attribute.name,client_contract_attribute.value from client_contract, client, client_contract_attribute , (select  stddev(value) as dev, avg(value) as mean from  client_contract_attribute where name = '"+column+"') as devtable where client_contract_attribute.name = '"+column+"' and  abs((client_contract_attribute.value - devtable.mean)) > (4 *devtable.dev) and client_contract.id = client_contract_attribute.client_contract_id and client_contract.is_active = true and client.code=client_contract.client_code";
print std_dev_query;
new_cursor.execute(std_dev_query);
for newlist in new_cursor.fetchall():
    for entry in newlist :
        fp.write(str(entry))
        fp.write(",")
    fp.write("\n");


std_dev_query = "select client_contract.client_code,prime_contract_route.code, charge_basis,contract_route_hash, trip_type , freight_charges,mean, dev,client_contract.service_type from client_contract, prime_contract_route , (select  stddev(freight_charges) as dev, avg(freight_charges) as mean from prime_contract_route where status in('ACTIVE','APPROVED', 'EXPIRED') and charge_basis not in ('Higher of Weight or Vehicle Capacity','Per Trip', 'PER KM', 'Per Kg') )as devtable where charge_basis not in ('Higher of Weight or Vehicle Capacity','Per Trip','PER KM', 'Per Kg') and  ((freight_charges - devtable.mean) > (4 *devtable.dev)  or freight_charges <(devtable.mean * .05)  )and prime_contract_route.status in('ACTIVE','APPROVED', 'EXPIRED') and prime_contract_route.contract_id=client_contract.id";

#std_dev_query = "select client_contract.code, client_contract.service_type, client_contract.client_code,client_contract_id,client_contract_attribute.name,client_contract_attribute.value from client_contract, client, client_contract_attribute , (select  stddev(value) as dev, avg(value) as mean from  client_contract_attribute where name = '"+column+"') as devtable where client_contract_attribute.name = '"+column+"' and  abs((client_contract_attribute.value - devtable.mean)) > (4 *devtable.dev) and client_contract.id = client_contract_attribute.client_contract_id and client_contract.is_active = true and client.code=client_contract.client_code";
print std_dev_query;
new_cursor.execute(std_dev_query);
for newlist in new_cursor.fetchall():
    for entry in newlist :
        fp.write(str(entry))
        fp.write(",")
    fp.write("\n");

fp.close();

print "process done";
