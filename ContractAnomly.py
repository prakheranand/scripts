import pymysql
from decimal import Decimal
import sys, traceback
import csv

#database connection
connection = pymysql.connect(host="finance-prod-replica.c90oafloutlv.ap-southeast-1.rds.amazonaws.com",
                             user="root",
                             passwd="f1n4nc3#r1v1g0",
                             database="contract_management_system" )

col_of_interest = "select distinct(name) from client_contract_attribute"

cursor = connection.cursor()
nameSize = cursor.execute(col_of_interest);
name_of_interest = [];
valueCursor = connection.cursor();
z = 0

for row in cursor.fetchall():
    valueQuery = "select value from client_contract_attribute_rectified where name = '" + row[0] + "' limit 1";
    try :
        valueCursor.execute(valueQuery);
        for singlerow in valueCursor.fetchall() :
            try :
                float(singlerow[0]);
                if(row[0] == 'BANK_ACCOUNT_NUMBER'):
                    continue;
                name_of_interest.append(row[0]);
                z = 1;
            except Exception as e1 :
                z = 0;
    except Exception as e :
        print (e);

new_cursor = connection.cursor();

final_list = [];
fp = open('/tmp/new_rectified_1.csv', 'w')
fp.write("client_contract_id,client_contract.code, service_type, client_code,attribute_name,attribute_value, is_active");
fp.write("\n");

d = dict();
for column in name_of_interest :
    #std_dev_query = "select name, value from client_contract_attribute , (select  stddev(value) as dev, avg(value) as mean from  client_contract_attribute where name = 'FLAT_RATE_PER_KG_HANDLING_CHARGES') as devtable where name = 'FLAT_RATE_PER_KG_HANDLING_CHARGES' and  abs((value - devtable.mean)) > (4 *devtable.dev)";

    #std_dev_query = "select client_contract_rectified.id, client_contract_rectified.code, client_contract_rectified.service_type, client_contract_rectified.client_code,client_contract_id,client_contract_attribute_rectified.name,client_contract_attribute_rectified.value from client_contract_rectified, client, client_contract_attribute_rectified , (select  stddev(value) as dev, avg(value) as mean from  client_contract_attribute_rectified where name = '"+column+"') as devtable where client_contract_attribute_rectified.name = '"+column+"' and  abs((client_contract_attribute_rectified.value - devtable.mean)) > (4 *devtable.dev) and client_contract_rectified.id = client_contract_attribute_rectified.client_contract_id and client_contract_rectified.is_active = true and client.code=client_contract_rectified.client_code";

    std_dev_query = "select client_contract_rectified.id, client_contract_rectified.code, client_contract_rectified.service_type, client_contract_rectified.client_code,client_contract_attribute_rectified.name,client_contract_attribute_rectified.value,client_contract_rectified.is_active from client_contract_rectified, client, client_contract_attribute_rectified , " \
                    "(select  stddev(client_contract_attribute_rectified.value) as dev, avg(client_contract_attribute_rectified.value) as mean from client_contract_rectified, client_contract_attribute_rectified where name = '"+column+"' and client_contract_attribute_rectified.client_contract_id=client_contract_rectified.id and client_contract_rectified.service_type in ('ZOOM_CORPORATE') and client_contract_rectified.status in ('ACTIVE', 'APPROVED', 'EXPIRED') and client_contract_rectified.is_active = 1) as devtable " \
                                                                                                                                                                                                                                          "where client_contract_attribute_rectified.name = '"+column+"' and  abs((client_contract_attribute_rectified.value - devtable.mean)) > (4 *devtable.dev) and client_contract_rectified.id = client_contract_attribute_rectified.client_contract_id and client.code=client_contract_rectified.client_code and client_contract_rectified.service_type in ('ZOOM_CORPORATE') and client_contract_rectified.status in ('ACTIVE', 'APPROVED', 'EXPIRED') and client_contract_rectified.is_active = 1";
    print std_dev_query;
    new_cursor.execute(std_dev_query);
    for newlist in new_cursor.fetchall():
        for entry in newlist :
            print(str(entry));
            fp.write(str(entry))
            fp.write(",")
        fp.write("\n");
fp.close();
print "process done";
