#!/bin/bash
set -e
date

src_dbhost='finance-prod-replica.c90oafloutlv.ap-southeast-1.rds.amazonaws.com';
src_dbuser='root';
src_dbpwd='f1n4nc3#r1v1g0';
src_dbname='rivigo_invoicing';

filename='/tmp/dump.sql';

#dest_host='';
#dest_dbuser='';
#dest_dbpwd='';
#dest_dbname='';
#fulltablesTest='schema_version';

fulltables='schema_version email_template state state_approver parent_client client client_address client_spoc client_contract client_contract_attribute prime_contract_route prime_contract_route_detail zoom_lane entity_action_ledger sequence';

fulltables='invoice';

mysqldump --no-data --host=$src_dbhost --user=$src_dbuser --password=$src_dbpwd $src_dbname > $filename;
mysqldump --complete-insert --no-create-info --host=$src_dbhost --user=$src_dbuser --password=$src_dbpwd $src_dbname $fulltables >> $filename;
#mysql -h $dest_host -u $dest_dbuser -p $dest_dbpwd $dest_dbname < $filename;

